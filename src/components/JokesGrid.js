
import {JokeCard} from './JokeCard'
import ChuckNorrisImages from '../assets/img/chuckNorrisImages.json'

export const JokesGrid = ({jokes}) => {

  const _renderJoke = (joke) => {
    const {id, value} = joke
    const key = `chuck-norris-joke-${joke.id}`
    const imageUrl = ChuckNorrisImages[Math.floor(Math.random() * ChuckNorrisImages.length)];
    return <JokeCard key={key} imageUrl={imageUrl} title={`ID: ${id}`} joke={value}/>
  }

  return <div className='Jokes-Cards'>{jokes.map(_renderJoke)}</div>
}