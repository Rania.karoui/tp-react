import {Card} from 'react-bootstrap'

export const JokeCard = ({imageUrl, title, joke}) => <Card style={{ width: '18rem', margin:'1rem' }}>
  <Card.Img variant="top" src={imageUrl} />
  <Card.Body>
    <Card.Title>{title}</Card.Title>
    <Card.Text>{joke}</Card.Text>
  </Card.Body>
</Card>