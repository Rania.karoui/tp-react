const API_URL = 'https://api.chucknorris.io'

export const getRandomJoke = async () => {
  const result = await fetch(`${API_URL}/jokes/random`);
  const joke = await result.json();
  return joke
}