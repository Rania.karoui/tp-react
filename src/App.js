import {useEffect, useState} from 'react';
import {getRandomJoke} from './api/ChuckNorrisAPI'
import {JokesGrid} from './components/JokesGrid';
import './App.css';

const App = () => {

  const [jokes, setJokes] = useState([])

  const _getJokes = async (numberOfJokes) => {
    let _jokes = []
    for(let i=0 ; i<numberOfJokes; i++) {
      const joke = await getRandomJoke()
      _jokes.push(joke)
    }
    setJokes(_jokes)
  }

  useEffect(() =>{
    _getJokes(10)
  },[])

  return (
    <div className="App">
      <div className='App-Header'><h1>Chunk Noris Facts</h1></div>
      <JokesGrid jokes={jokes} />
    </div>
  );
}

export default App;
